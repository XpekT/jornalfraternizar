# Jornal Fraternizar
Projeto pro-bono desenvolvido para o Jornal Fraternizar.
Tecnologias usadas foram slim framework + VueJS

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)