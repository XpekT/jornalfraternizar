<?php

namespace App\Classes;

use Swift_Mailer as Mailer;
use Swift_SmtpTransport as Transport;
use Swift_Message as Message;

class Mail
{

  protected $transport;
  protected $mailer;
  protected $mode = [
    "development" => "http://localhost:5000/users/activate/",
    "production" => "http://jornalfraternizar.dx.am/users/activate/"
  ];

  public function __construct()
  {
    $this->transport = Transport::newInstance('')
                                ->setUsername('')
                                ->setPassword('');

    $this->mailer = Mailer::newInstance($this->transport);
  }

  public function activate($email, $username, $code, $password)
  {

    $message = Message::newInstance('Activação de Conta')
                      ->setFrom(['accounts@jornalfraternizar.dx.am' => 'Direcção'])
                      ->setTo([$email => $username])
                      ->setBody(
                        '<h1>Activação de Conta<h1>
                         <p>Olá, '. $username .'!</p>
                         <br>
                         <p>Palavra-Passe: <strong>'. $password .'</strong></p>
                         <p style="color: red;">Não se esqueça de mudar a palavra-passe!</p>
                         <br>
                         <p>Clique no link para activar a sua conta:</p>
                         <br>
                         <a href="'. $this->mode["development"] . $email .'/'. $code .'">Activar Conta</a>
                         <br><br>
                         Direcção do Jornal Fraternizar.
                        '
                      , 'text/html');
    
    return $this->mailer->send($message);

  }

  public function recover($email, $username, $password)
  {

    $message = Message::newInstance('Recuperação de Conta')
                      ->setFrom(['accounts@jornalfraternizar.dx.am' => 'Direcção'])
                      ->setTo([$email => $username])
                      ->setBody(
                        '<h1>Recuperação de Conta<h1>
                         <p>Olá, '. $username .'!</p>
                         <br>
                         <p>A sua palavra-passe foi mudada para:</p>
                         <br>
                         <p><strong>'. $password .'</strong></p>
                         <br>
                         <p style="color: red">Não se esqueça de mudar a palavra-passe assim que entrar no seu perfil!</p>
                         <br><br>
                         Direcção do Jornal Fraternizar.
                        '
                      , 'text/html');
    
    return $this->mailer->send($message);

  }

}