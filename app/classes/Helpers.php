<?php

namespace App\Classes;

class Helpers
{

  public static function sanitize($data)
	{
    if(is_array($data)) {
      foreach ($data as $index => $value) {
        $data[$index] = htmlentities(trim($value), ENT_QUOTES, "UTF-8");
      }
    }

    if(is_string($data)) {
      $data = htmlentities(trim($data), ENT_QUOTES, "UTF-8");
    }

    return $data;
  }

  public static function message($mode, $message = "")
  {
    if($mode === 'error') {
      if($message === "") {
        return "Oops! Alguma coisa correu mal! Tente novamente mais tarde!";
      } else {
        return $message;
      } 
    }

    if($mode === 'success') {
      if($message === "") {
        return "Operação Realizada com Successo!";
      } else {
        return $message;
      }
    }
  }

  public static function isEmpty($string)
  {
    return mb_strlen($string) == 0;
  }

  public static function getIp()
  {
    $ipaddress = '';

    if (isset($_SERVER['HTTP_CLIENT_IP']))
    
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
    
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    
    else if(isset($_SERVER['HTTP_FORWARDED']))
    
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    
    else if(isset($_SERVER['REMOTE_ADDR']))
    
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    
    else
    
        $ipaddress = 'UNKNOWN';
 
    return $ipaddress;
  }

}