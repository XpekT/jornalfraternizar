<?php

namespace App\Classes;

class Session
{

  public static function set($name, $data = [])
  {
    $_SESSION[$name] = $data;
  }

  public static function get($name)
  {
    return isset($_SESSION[$name]) ? $_SESSION[$name] : NULL;
  }

  public static function clear($name)
  {
    unset($_SESSION[$name]);
  }

  public static function hasSession($name)
  {
    return isset($_SESSION[$name]);
  }

}