<?php

require "vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use App\Classes\Database;

$db = new Database([
  "driver" => "mysql",
  "host" => "127.0.0.1",
  "database" => "jornalfraternizar",
  "username" => "root",
  "password" => "root",
  "charset" => "utf8",
  "collation" => "utf8_unicode_ci",
  "prefix" => ""
]);

$db->start();

//Creating schema
Capsule::schema()->create("articles", function (Blueprint $table) {
    $table->increments("id");
    $table->string("title");
    $table->string("category");
    $table->integer("edition");
    $table->text("text");
    $table->string("author");
    $table->text("slug");
    $table->integer("user_id")->references('id')->on('users');
    $table->timestamps();
});