<?php

require "vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use App\Classes\Database;

$db = new Database([
  "driver" => "mysql",
  "host" => "127.0.0.1",
  "database" => "jornalfraternizar",
  "username" => "root",
  "password" => "root",
  "charset" => "utf8",
  "collation" => "utf8_unicode_ci",
  "prefix" => ""
]);

$db->start();

//Creating schema
Capsule::schema()->create("users", function (Blueprint $table) {
    $table->increments("id");
    $table->string("email");
    $table->string("username");
    $table->string("password");
    $table->tinyInteger("active")->default("0");
    $table->string("role");
    $table->string("code")->nullable()->default(NULL);
    $table->timestamp("last_session_at")->nullable()->default(NULL);
    $table->timestamps();
});