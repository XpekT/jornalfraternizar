<?php

namespace App\Middleware;

use App\Classes\Session;

class Auth
{
  
  public function __invoke($request, $response, $next)
  {
    if(!Session::hasSession('userSession')) {

      $response = $next($request, $response->withStatus(302)->withHeader('Location', '/users/login'));

      return $response; 
    
    }

    $response = $next($request, $response);

    return $response;

  }

}