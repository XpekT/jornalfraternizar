<?php

namespace App\Middleware;

use App\Classes\Session;

class Admin
{
  
  public function __invoke($request, $response, $next)
  {
    if(!Session::hasSession('userSession')) {

      $response = $next($request, $response->withStatus(302)->withHeader('Location', '/users/login'));

      return $response; 
    
    }

    if(Session::get('userSession')['role'] != 'Administrador') {

      $response = $next($request, $response->withStatus(302)->withHeader('Location', '/home'));

      return $response;

    }

    $response = $next($request, $response);

    return $response;

  }

}