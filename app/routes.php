<?php

require "bootstrap.php";

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Middleware\Auth;
use App\Middleware\Admin;

// Root Home Redirect Route - Redirects to the Home Route if Root is requested
$app->get("/", function (Request $request, Response $response) {
  return $response->withHeader("Location", "home");
});

// Home Route
$app->get("/home", "HomeController:index")->setName("home");

// Error Route
$app->get("/error", "ErrorController:index")->setName("error");

// User Group
$app->group("/users", function() {

  // Activate User - GET
  $this->get("/activate/{email}/{code}", "UserController:activate");

  // Create User - POST
  $this->post("/create", "UserController:create")->add(new Admin);

  // Login User - POST
  $this->post("/login", "UserController:login");

  // Logout User - GET
  $this->get("/logout", "UserController:logout");

  // User Profile - GET
  $this->get("/profile", "UserController:profile")->add(new Auth);

  // User Delete - POST
  $this->post("/user/delete", "UserController:delete")->add(new Auth);

  // User Session
  $this->get("/session", "UserController:session");

  // Recover Password - POST
  $this->post("/recover", "UserController:recover");

  // Edit Profile - POST
  $this->post("/profile/edit", "UserController:edit");

  // Get all user's collectibles
  $this->post("/user/collectibles", "UserController:collectibles");

});

// Articles Group
$app->group("/articles", function() {

  // Create Article - GET
  $this->get("/create", "ArticleController:index")->add(new Auth);

  // Create Article - POST
  $this->post("/create", "ArticleController:create")->add(new Auth);

  // Single Article - GET
  $this->get("/article/{slug}", "ArticleController:article");

  // Delete Article - POST
  $this->post("/article/delete", "ArticleController:delete")->add(new Auth);

  // Edit Article - POST
  $this->post("/article/edit", "ArticleController:edit")->add(new Auth);

});

// Editions Group
$app->group("/editions", function() {

  // Editions Index - GET
  $this->get('/index', "EditionController:index");

  // Edition - GET
  $this->get('/edition/{number}', "EditionController:edition");

});

// Comments Group
$app->group("/comments", function() {

  // Comment Article Add - POST
  $this->post("/article/add", "CommentController:articleAdd");

  // Comment Article Remove - POST
  $this->post("/article/remove", "CommentController:articleRemove")->add(new Auth);

  // Comment Book Add - POST
  $this->post("/book/add", "CommentController:bookAdd");

  // Comment Book Remove - POST
  $this->post("/book/remove", "CommentController:bookRemove")->add(new Auth);

});

// Likes - Group
$app->group("/likes", function() {

  // Article Like - POST
  $this->post("/article/like", "LikeController:likeArticle");

  // Book Like - POST
  $this->post("/book/like", "LikeController:likeBook");
  

});

// Search - POST
$app->post("/search", "SearchController:search");

// Admin - Group
$app->group("/admin", function() {

  // Index - GET
  $this->get('/index', "AdminController:index");

  // Stats - POST
  $this->post('/stats', "AdminController:stats");

})->add(new Admin);

// Books Group
$app->group("/books", function() {

  // Books Index - POST
  $this->get("/index", "BookController:index");

  // Book - GET
  $this->get("/book/{slug}", "BookController:single");

  // All Books - POST
  $this->post("/all", "BookController:all");

  // Add Book - POST
  $this->post("/book/add", "BookController:add")->add(new Admin);

  // Edit Book - POST
  $this->post("/book/edit", "BookController:edit")->add(new Admin);

  // Delete Book - POST
  $this->post("/book/delete", "BookController:delete")->add(new Admin);

});

// Videos Group
$app->group("/videos", function() {

  $this->get("/all", "VideoController:all");

  // Add Video - POST
  $this->post("/video/add", "VideoController:add")->add(new Admin);

  // Edit Video - POST
  $this->post("/video/edit", "VideoController:edit")->add(new Admin);

  // Delete Video - POST
  $this->post("/video/delete", "VideoController:delete")->add(new Admin);

});

// BIO - GET
$app->get("/bio", "BioController:index");

// Visitor Count - GET
$app->get("/visitors", "VisitorController:counter");

// Archive Route - GET
$app->get("/archive", "ArchiveController:index");

// Archive File API - POST
$app->post("/archive/api/{year}/{number}", "ArchiveController:item");

// Test Route
//$app->get("/test", "TestController:get");