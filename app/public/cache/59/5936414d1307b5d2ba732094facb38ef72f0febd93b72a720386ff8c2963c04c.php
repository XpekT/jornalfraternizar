<?php

/* /books/index.html.twig */
class __TwigTemplate_1d875450268d0a03be20f20cafe63b82f4cb6bcc4d348b40bf946b8664daca92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "/books/index.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "
  <app page=\"books\"></app>

";
    }

    public function getTemplateName()
    {
        return "/books/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block main %}

  <app page=\"books\"></app>

{% endblock %}", "/books/index.html.twig", "C:\\Apache24\\htdocs\\calkya\\app\\public\\templates\\books\\index.html.twig");
    }
}
