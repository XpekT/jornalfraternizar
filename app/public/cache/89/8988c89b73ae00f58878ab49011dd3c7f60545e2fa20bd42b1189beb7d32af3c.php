<?php

/* /articles/create.html.twig */
class __TwigTemplate_b1c86d9fbbb32b4d34dbbe57aa3033bd4fdba79a5e0837cb7b28d3f4feb64238 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "/articles/create.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "
  <app page=\"article-create\"></app>

";
    }

    public function getTemplateName()
    {
        return "/articles/create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block main %}

  <app page=\"article-create\"></app>

{% endblock %}", "/articles/create.html.twig", "C:\\Apache24\\htdocs\\calkya\\app\\public\\templates\\articles\\create.html.twig");
    }
}
