<?php

/* /editions/index.html.twig */
class __TwigTemplate_6b4bda0fda448316fe4e6a62f29aa75b7ca63a41d6be5d597e51b32761fa4c95 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "/editions/index.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "
  <app page=\"editions-index\" :editions=\"";
        // line 5
        echo twig_escape_filter($this->env, json_encode((isset($context["editions"]) || array_key_exists("editions", $context) ? $context["editions"] : (function () { throw new Twig_Error_Runtime('Variable "editions" does not exist.', 5, $this->getSourceContext()); })())), "html", null, true);
        echo "\"></app>

";
    }

    public function getTemplateName()
    {
        return "/editions/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block main %}

  <app page=\"editions-index\" :editions=\"{{ editions|json_encode }}\"></app>

{% endblock %}", "/editions/index.html.twig", "C:\\Apache24\\htdocs\\calkya\\app\\public\\templates\\editions\\index.html.twig");
    }
}
