<?php

/* footer.html.twig */
class __TwigTemplate_ffc6336c6b913dfaf3b07db833f18ea8bb1a8cbe7440b1e273f094b0cedb13c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"footer\">
  <div class=\"main-footer\">
    <div class=\"left navigation\">
      <h3>Mapa do Site</h3>
      <ul>
        <a href=\"/home\"><li>Home</li></a>
        <a href=\"/editions/index\"><li>Edições</li></a>
        <a href=\"/books/index\"><li>Livros</li></a>
        <a href=\"/videos/all\"><li>Vídeos</li></a>
        <a href=\"/bio\"><li>Biografia</li></a>
        <a href=\"/archive\"><li>Arquivo</li></a>
      </ul>
    </div>
    <div class=\"separator\"></div>
    <div class=\"middle social-links\">
      <h3>Social</h3>
      <div class=\"image-container\">
        <a href=\"https://www.facebook.com/mario.p.deoliveira\" target=\"_blank\"><img src=\"/assets/images/logos/facebook-logo.png\" alt=\"facebook\"></a>
        <a href=\"https://twitter.com/padremario1\" target=\"_blank\"><img src=\"/assets/images/logos/twitter-logo.png\" alt=\"twitter\"></a>
        <a href=\"https://www.youtube.com/user/PadreMarioOliveira\" target=\"_blank\"><img src=\"/assets/images/logos/youtube-logo.png\" alt=\"youtube\"></a>
        <a href=\"mailto:padremario@sapo.pt\"><img src=\"/assets/images/logos/email-logo.png\" alt=\"email\"></a>
      </div>
    </div>
    <div class=\"separator\"></div>
    <div class=\"right disclaimer\">
      <h3>Jornal Fraternizar</h3>
      <p>Rua Alto da Paixão nº 298 4615-418 MACIEIRA DA LIXA</p>
      <p>Contactos: 255 49 63 58 | 933 93 65 02 | 968 07 81 22</p>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"footer\">
  <div class=\"main-footer\">
    <div class=\"left navigation\">
      <h3>Mapa do Site</h3>
      <ul>
        <a href=\"/home\"><li>Home</li></a>
        <a href=\"/editions/index\"><li>Edições</li></a>
        <a href=\"/books/index\"><li>Livros</li></a>
        <a href=\"/videos/all\"><li>Vídeos</li></a>
        <a href=\"/bio\"><li>Biografia</li></a>
        <a href=\"/archive\"><li>Arquivo</li></a>
      </ul>
    </div>
    <div class=\"separator\"></div>
    <div class=\"middle social-links\">
      <h3>Social</h3>
      <div class=\"image-container\">
        <a href=\"https://www.facebook.com/mario.p.deoliveira\" target=\"_blank\"><img src=\"/assets/images/logos/facebook-logo.png\" alt=\"facebook\"></a>
        <a href=\"https://twitter.com/padremario1\" target=\"_blank\"><img src=\"/assets/images/logos/twitter-logo.png\" alt=\"twitter\"></a>
        <a href=\"https://www.youtube.com/user/PadreMarioOliveira\" target=\"_blank\"><img src=\"/assets/images/logos/youtube-logo.png\" alt=\"youtube\"></a>
        <a href=\"mailto:padremario@sapo.pt\"><img src=\"/assets/images/logos/email-logo.png\" alt=\"email\"></a>
      </div>
    </div>
    <div class=\"separator\"></div>
    <div class=\"right disclaimer\">
      <h3>Jornal Fraternizar</h3>
      <p>Rua Alto da Paixão nº 298 4615-418 MACIEIRA DA LIXA</p>
      <p>Contactos: 255 49 63 58 | 933 93 65 02 | 968 07 81 22</p>
    </div>
  </div>
</div>", "footer.html.twig", "C:\\Apache24\\htdocs\\jornalfraternizar\\app\\public\\templates\\footer.html.twig");
    }
}
