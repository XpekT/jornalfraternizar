<?php

/* base.html.twig */
class __TwigTemplate_508f5f390e8d887234a6df465c97507061b292dcc5bb05e1896cab31d9d7e635 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html class=\"no-js\" lang=\"pt\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
        <title>Jornal Fraternizar</title>
        <meta name=\"description\" content=\"Jornal Online Religioso - Padre Mário Pais de Oliveira - Padre da Lixa\">
        <meta name=\"robots\" content=\"index,follow\"/>
        <meta http-equiv=\"content-language\" content=\"pt-pt\" />
        <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />
        <meta name=\"keywords\" content=\"Fátima, Igreja, Política\" />
        <meta name=\"author\" content=\"Bruno Moreira\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"/assets/images/favicons/apple-icon-57x57.png\">
        <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"/assets/images/favicons/apple-icon-60x60.png\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"/assets/images/favicons/apple-icon-72x72.png\">
        <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"/assets/images/favicons/apple-icon-76x76.png\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"/assets/images/favicons/apple-icon-114x114.png\">
        <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"/assets/images/favicons/apple-icon-120x120.png\">
        <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"/assets/images/favicons/apple-icon-144x144.png\">
        <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"/assets/images/favicons/apple-icon-152x152.png\">
        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"/assets/images/favicons/apple-icon-180x180.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"/assets/images/favicons/android-icon-192x192.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"/assets/images/favicons/favicon-32x32.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"/assets/images/favicons/favicon-96x96.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"/assets/images/favicons/favicon-16x16.png\">
        <link rel=\"manifest\" href=\"/assets/images/favicons/manifest.json\">
        <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
        <meta name=\"msapplication-TileImage\" content=\"/ms-icon-144x144.png\">
        <meta name=\"theme-color\" content=\"#ffffff\">

        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css\">
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js\"></script>
        <link rel=\"stylesheet\" href=\"/assets/styles/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"/assets/styles/quill-core.min.css\">
        <link rel=\"stylesheet\" href=\"/assets/styles/main.min.css\">
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <span id=\"csrf_name\" name=\"csrf_name\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 46, $this->getSourceContext()); })()), "csrf_name", array()), "html", null, true);
        echo "\"></span>
        <span id=\"csrf_value\" name=\"csrf_value\" value=\"";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 47, $this->getSourceContext()); })()), "csrf_value", array()), "html", null, true);
        echo "\"></span>

        <div class=\"container-fluid\" id=\"app\" v-cloak>
            <navbar></navbar>
            ";
        // line 51
        $this->displayBlock('main', $context, $blocks);
        // line 52
        echo "            <loading></loading>
            <modal></modal>
            <search></search>
            ";
        // line 55
        $this->loadTemplate("footer.html.twig", "base.html.twig", 55)->display($context);
        // line 56
        echo "            
            <div id=\"scroll-to-top\" v-scroll-to=\"{ element: 'body', offset: 0, duration: 600, easing: [.13,.77,.39,.5] }\">
                <span class=\"glyphicon glyphicon-menu-up\"></span>
            </div>

        </div>

        <script type=\"text/javascript\" src=\"/assets/js/jquery.min.js\"></script>
        <script type=\"text/javascript\" src=\"/assets/js/bootstrap.min.js\"></script>
        <script type=\"text/javascript\" src=\"/assets/js/bundle.min.js\"></script>

    </body>
</html>";
    }

    // line 51
    public function block_main($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 51,  87 => 56,  85 => 55,  80 => 52,  78 => 51,  71 => 47,  67 => 46,  20 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!doctype html>
<html class=\"no-js\" lang=\"pt\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
        <title>Jornal Fraternizar</title>
        <meta name=\"description\" content=\"Jornal Online Religioso - Padre Mário Pais de Oliveira - Padre da Lixa\">
        <meta name=\"robots\" content=\"index,follow\"/>
        <meta http-equiv=\"content-language\" content=\"pt-pt\" />
        <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />
        <meta name=\"keywords\" content=\"Fátima, Igreja, Política\" />
        <meta name=\"author\" content=\"Bruno Moreira\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"/assets/images/favicons/apple-icon-57x57.png\">
        <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"/assets/images/favicons/apple-icon-60x60.png\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"/assets/images/favicons/apple-icon-72x72.png\">
        <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"/assets/images/favicons/apple-icon-76x76.png\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"/assets/images/favicons/apple-icon-114x114.png\">
        <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"/assets/images/favicons/apple-icon-120x120.png\">
        <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"/assets/images/favicons/apple-icon-144x144.png\">
        <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"/assets/images/favicons/apple-icon-152x152.png\">
        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"/assets/images/favicons/apple-icon-180x180.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"/assets/images/favicons/android-icon-192x192.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"/assets/images/favicons/favicon-32x32.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"/assets/images/favicons/favicon-96x96.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"/assets/images/favicons/favicon-16x16.png\">
        <link rel=\"manifest\" href=\"/assets/images/favicons/manifest.json\">
        <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
        <meta name=\"msapplication-TileImage\" content=\"/ms-icon-144x144.png\">
        <meta name=\"theme-color\" content=\"#ffffff\">

        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css\">
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js\"></script>
        <link rel=\"stylesheet\" href=\"/assets/styles/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"/assets/styles/quill-core.min.css\">
        <link rel=\"stylesheet\" href=\"/assets/styles/main.min.css\">
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <span id=\"csrf_name\" name=\"csrf_name\" value=\"{{ token.csrf_name }}\"></span>
        <span id=\"csrf_value\" name=\"csrf_value\" value=\"{{ token.csrf_value }}\"></span>

        <div class=\"container-fluid\" id=\"app\" v-cloak>
            <navbar></navbar>
            {% block main %}{% endblock %}
            <loading></loading>
            <modal></modal>
            <search></search>
            {% include \"footer.html.twig\" %}
            
            <div id=\"scroll-to-top\" v-scroll-to=\"{ element: 'body', offset: 0, duration: 600, easing: [.13,.77,.39,.5] }\">
                <span class=\"glyphicon glyphicon-menu-up\"></span>
            </div>

        </div>

        <script type=\"text/javascript\" src=\"/assets/js/jquery.min.js\"></script>
        <script type=\"text/javascript\" src=\"/assets/js/bootstrap.min.js\"></script>
        <script type=\"text/javascript\" src=\"/assets/js/bundle.min.js\"></script>

    </body>
</html>", "base.html.twig", "C:\\Apache24\\htdocs\\calkya\\app\\public\\templates\\base.html.twig");
    }
}
