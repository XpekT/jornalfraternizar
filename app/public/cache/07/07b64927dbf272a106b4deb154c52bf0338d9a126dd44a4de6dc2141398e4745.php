<?php

/* /articles/single.html.twig */
class __TwigTemplate_45ce6afbe147abf689750b1f566885ed40c44ef14cb0a5c01a284424bdd718cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "/articles/single.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "
  <app page=\"article-single\" :article=\"";
        // line 5
        echo twig_escape_filter($this->env, json_encode((isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new Twig_Error_Runtime('Variable "article" does not exist.', 5, $this->getSourceContext()); })())), "html", null, true);
        echo "\"></app>

";
    }

    public function getTemplateName()
    {
        return "/articles/single.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block main %}

  <app page=\"article-single\" :article=\"{{ article|json_encode }}\"></app>

{% endblock %}", "/articles/single.html.twig", "C:\\Apache24\\htdocs\\calkya\\app\\public\\templates\\articles\\single.html.twig");
    }
}
