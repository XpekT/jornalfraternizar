<?php

/* /editions/single.html.twig */
class __TwigTemplate_9ed3d56396c0e5ef2bd20c6178c78b988071332b1786d9dfcc88fa9f0e9d9539 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "/editions/single.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "
  <app page=\"editions-single\" :edition=\"";
        // line 5
        echo twig_escape_filter($this->env, json_encode((isset($context["edition"]) || array_key_exists("edition", $context) ? $context["edition"] : (function () { throw new Twig_Error_Runtime('Variable "edition" does not exist.', 5, $this->getSourceContext()); })())), "html", null, true);
        echo "\"></app>

";
    }

    public function getTemplateName()
    {
        return "/editions/single.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block main %}

  <app page=\"editions-single\" :edition=\"{{ edition|json_encode }}\"></app>

{% endblock %}", "/editions/single.html.twig", "C:\\Apache24\\htdocs\\calkya\\app\\public\\templates\\editions\\single.html.twig");
    }
}
