<?php

/* /users/admin.html.twig */
class __TwigTemplate_ce2edb7dac77fb8cbcbe85e781a5c6e73d1b2aecac2da4a51b3cb2f0d0dea45e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "/users/admin.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "
  <app page=\"admin\"></app>

";
    }

    public function getTemplateName()
    {
        return "/users/admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block main %}

  <app page=\"admin\"></app>

{% endblock %}", "/users/admin.html.twig", "C:\\Apache24\\htdocs\\calkya\\app\\public\\templates\\users\\admin.html.twig");
    }
}
