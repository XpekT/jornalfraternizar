<?php

/* /users/profile.html.twig */
class __TwigTemplate_1e915ad46bd06d0c7fde35dcd1bca89825f6f7ec60f0e8b9df7121620aad284c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "/users/profile.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "  
  <app page=\"profile\"></app>

";
    }

    public function getTemplateName()
    {
        return "/users/profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block main %}
  
  <app page=\"profile\"></app>

{% endblock main %}", "/users/profile.html.twig", "C:\\Apache24\\htdocs\\calkya\\app\\public\\templates\\users\\profile.html.twig");
    }
}
