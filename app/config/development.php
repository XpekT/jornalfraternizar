<?php

require "../../vendor/autoload.php";

use App\Classes\Database;
use Slim\Views\Twig;
use Slim\Csrf\Guard;
use Slim\Views\TwigExtension;
use Slim\App;
use Slim\Container;
use Valitron\Validator;
use App\Classes\Helpers;

// Language for Validator
Validator::langDir('../../vendor/vlucas/valitron/lang/');
Validator::lang('pt-br');

// Start Sessions if necessary
if (session_status() == PHP_SESSION_NONE) {
    session_start();
    session_regenerate_id(true);
}

// Set default timezone
date_default_timezone_set("Europe/Lisbon");

$config = [
  "development" => [
    "displayErrorDetails" => true,
    "addContentLengthHeader" => false,
    "templateDebug" => true
  ],
  "production" => [
    "displayErrorDetails" => false,
    "addContentLengthHeader" => false,
    "templateDebug" => false
  ]
];

// Instantiate Slim with config - Ensure "production" mode when releasing the project
$app = new App([
  "settings" => $config["development"]
]);

$container = $app->getContainer();
$container["csrf"] = function ($c) {
    return new Guard;
};

// Add the CSRF Package
$app->add(new Guard);

// Add twig templating system
$container["view"] = function($container) use($config) {

  $view = new Twig("../public/templates", [
    "cache" => "../public/cache",
    "strict_variables" => true,
    "debug" => $config['development']['templateDebug']
  ]);

  $basePath = rtrim(str_ireplace("index.php", "", $container["request"]->getUri()->getBasePath()), "/");
  $view->addExtension(new TwigExtension($container["router"], $basePath));

  return $view;
};

// Loader that fetches the static archive pages
$container["fileLoader"] = function($container) use ($config) {

    $loader = new Twig_Loader_Filesystem('../public/templates');
    
    $twig = new Twig_Environment($loader, array(
        'cache' => '../public/cache',
    ));

    return $twig;
};

// Not Allowed Method Error Handler
$container['notAllowedHandler'] = function ($container) {
    return function ($request, $response, $methods) use ($container) {
        return $container['response']
            ->withStatus(405)
            ->withHeader('Location', '/error');
    };
};

// Not Found Error Handler
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return $container['response']
            ->withStatus(404)
            ->withHeader('Location', '/error');
    };
};

// PHP Error Handler
$container['phpErrorHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return $container['response']
            ->withStatus(500)
            ->withHeader('Location', '/error');
    };
};

// Add Controllers to DI Container
$container["HomeController"] = function ($container) {
    return new \App\Controllers\HomeController($container);
};

$container["ErrorController"] = function ($container) {
    return new \App\Controllers\ErrorController($container);
};

$container["UserController"] = function ($container) {
    return new \App\Controllers\UserController($container);
};

$container["ArticleController"] = function ($container) {
    return new \App\Controllers\ArticleController($container);
};

$container["EditionController"] = function ($container) {
    return new \App\Controllers\EditionController($container);
};

$container["CommentController"] = function ($container) {
    return new \App\Controllers\CommentController($container);
};

$container["LikeController"] = function ($container) {
    return new \App\Controllers\LikeController($container);
};

$container["SearchController"] = function ($container) {
    return new \App\Controllers\SearchController($container);
};

$container["AdminController"] = function ($container) {
    return new \App\Controllers\AdminController($container);
};

$container["BookController"] = function ($container) {
    return new \App\Controllers\BookController($container);
};

$container["VideoController"] = function ($container) {
    return new \App\Controllers\VideoController($container);
};

$container["BioController"] = function ($container) {
    return new \App\Controllers\BioController($container);
};

$container["VisitorController"] = function ($container) {
    return new \App\Controllers\VisitorController($container);
};

$container["ArchiveController"] = function ($container) {
    return new \App\Controllers\ArchiveController($container);
};

$container["TestController"] = function ($container) {
    return new \App\Controllers\TestController($container);
};

// Set default title
$container["title"] = "Jornal Fraternizar";

// Database Environment Settings
$container["database_development"] = function($container) {
  return [
    "driver" => "mysql",
    "host" => "127.0.0.1",
    "database" => "jornalfraternizar",
    "username" => "root",
    "password" => "root",
    "charset" => "utf8",
    "collation" => "utf8_unicode_ci",
    "prefix" => ""
  ];
};

$container["database_production"] = function($container) {
  return [
    "driver" => "",
    "host" => "",
    "database" => "",
    "username" => "",
    "password" => "",
    "charset" => "utf8",
    "collation" => "utf8_unicode_ci",
    "prefix" => ""
  ];
};

// Connect to Database
$db = new Database($container["database_development"]);

$db->start();

$container["relay"] = $db->getInstance();

// Register every visit to make a counter
$relay = $db->getInstance();
$ip_address = Helpers::getIp();

$visitor = $relay->table('visitors')
                  ->where('ip_address', $ip_address)
                  ->select('ip_address')
                  ->get()->first();

if($visitor == NULL) {

    $relay->table('visitors')->insert([
        'ip_address' => $ip_address
    ]);

}