<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;

use App\Models\Test;
use App\Classes\Session;
use Hautelook\Phpass\PasswordHash;

class TestController
{
  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function get(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $hash = new PasswordHash(12, false);
  
    return $hash->hashPassword("12345");
  }
}