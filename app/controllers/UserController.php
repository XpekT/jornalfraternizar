<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use App\Models\User;
use App\Classes\Session;

class UserController
{
  protected $container;
  protected $token;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function create(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $user = new User;

    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    return $res->withJson($user->create_user($req->getParsedBody(), $token));
  }

  public function activate(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $user = new User;

    $attributes = [
      "email" => $req->getAttribute('email'),
      "code" => $req->getAttribute('code'),
    ];

    if($user->activate_user($attributes)) {
      return $res->withHeader('Location', '/home');
    }

    return $res->withHeader('Location', '/error');
  }

  public function login(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $user = new User;

    return $res->withJson($user->login_user($req->getParsedBody(), $token));
  }

  public function logout(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    User::logout_user();

    return $res->withHeader('Location', '/');
  }

  public function profile(ServerRequestInterface $req, ResponseInterface $res, $args)
  {

    if(Session::hasSession('userSession')) {

      $nameKey = $this->container->csrf->getTokenNameKey();
      $valueKey = $this->container->csrf->getTokenValueKey();

      $token = [
        'csrf_name' => $req->getAttribute($nameKey),
        'csrf_value' => $req->getAttribute($valueKey)
      ];

      $userSession = Session::get("userSession");

      return $this->container->view->render($res, "/users/profile.html.twig", [
        "userSession" => $userSession,
        "token" => $token
      ]);

    } else {

      return $res->withHeader('Location', '/users/login');

    }
  }

  public function session(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    return $res->withJson(Session::get('userSession'));
  }

  public function recover(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $user = new User;

    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    return $res->withJson($user->recover_password($req->getParsedBody(), $token));
  }

  public function edit(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $user = new User;

    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    return $res->withJson($user->edit_profile($req->getParsedBody(), $token));
  }

  public function delete(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $user = new User;

    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    return $res->withJson($user->delete_profile($req->getParsedBody(), $token));
  }

  public function collectibles(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $user = new User;

    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    return $res->withJson($user->get_collectibles($req->getParsedBody(), $token));
  }

}