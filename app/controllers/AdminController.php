<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;

use App\Models\User;
use App\Models\Article;
use App\Models\Book;
use App\Models\Video;

class AdminController
{
  protected $container;
  protected $token;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function index(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    return $this->container->view->render($res, "/users/admin.html.twig", [
      'token' => $token
    ]);
  }

  public function stats(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $users = User::with('articles')->with('books')->with('videos')->get()->all();
    $articles = Article::all()->count();
    $books = Book::all()->count();
    $videos = Video::all()->count();

    return $res->withJson([
      "token" => $token,
      "stats" => [
        "users" => $users,
        "articles" => $articles,
        "books" => $books,
        "videos" => $videos
      ]
    ]);
  }

}