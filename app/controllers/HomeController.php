<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use App\Models\Article;
use App\Models\Book;
use App\Models\Video;

class HomeController
{
  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function index(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    // Articles - if user system is activated
    // $collab = Article::join('users', 'articles.user_id', '=', 'users.id')->where(function($query) {

    //   $query->where('role', 'Colaborador');

    // })->select('articles.*', 'users.username')->orderBy('articles.created_at', 'Desc')->take(10)->get();
    
    // $main = Article::join('users', 'articles.user_id', '=', 'users.id')->where(function($query) {

    //   $query->where('role', 'Administrador');

    // })->with('likes')->select('articles.*', 'users.username')->orderBy('articles.created_at', 'Desc')->take(8)->get();

    // Articles - if user system is not activated
    $collab = Article::with('user')->with('likes')->where('section', 'collab')->orderBy('edition', 'Desc')->take(10)->get();
    $main = Article::with('user')->with('likes')->where('section', 'main')->orderBy('edition', 'Desc')->take(10)->get();
    
    // Books
    $books = Book::orderBy('created_at', 'Desc')->select('image', 'slug', 'price')->take(5)->get();

    // Videos
    $videos = Video::orderBy('created_at', 'Desc')->select('title', 'link')->take(2)->get();

    return $this->container->view->render($res, "home.html.twig", [
      'token' => $token,
      'data' => [
        'articles' => [
          'collab' => $collab,
          'main' => $main
        ],
        'books' => $books,
        'videos' => $videos
      ]
    ]);
  }
}