<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use App\Models\Article;
use App\Models\Book;
use App\Models\Video;
use App\Classes\Helpers;

class SearchController 
{

  protected $container;
  protected $token;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function search(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $search = Helpers::sanitize($req->getParsedBody()['query']);

    $articles = Article::with('comments')->with('likes')
    ->where('title', 'like', '%'.$search.'%')
    ->orWhere('author', 'like', '%'.$search.'%')
    ->orWhere('category', 'like', '%'.$search.'%')
    ->orWhere('edition', 'like', '%'.$search.'%')
    ->orderBy('created_at', 'desc')
    ->take(5)
    ->get();

    $books = Book::with('comments')->with('likes')
    ->where('title', 'like', '%'.$search.'%')
    ->orWhere('author', 'like', '%'.$search.'%')
    ->orderBy('created_at', 'desc')
    ->take(5)
    ->get();

    $videos = Video::where('title', 'like', '%'. $search .'%')->take(5)->get();

    return $res->withJson([
      'results' => [
        'articles' => $articles,
        'books' => $books,
        'videos' => $videos
      ],
      'token' => $token
    ]);
  }

}