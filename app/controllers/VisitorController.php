<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;

class VisitorController
{
  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function counter(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $visitors = $this->container->relay->table('visitors')->count();

    return $res->withJson([
      'visitors' => $visitors
    ]);
  }
}