<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use App\Models\Video;

class VideoController
{
  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function all(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $videos = Video::with('user')->get()->all();

    return $this->container->view->render($res, "videos.html.twig", [
      'token' => $token,
      'videos' => $videos
    ]);
  }

  public function add(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $video = new Video;

    return $res->withJson($video->create_video($req->getParsedBody(), $token));
  }

  public function edit(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $video = new Video;

    return $res->withJson($video->edit_video($req->getParsedBody(), $token));
  }

  public function delete(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $video = new Video;

    return $res->withJson($video->delete_video($req->getParsedBody(), $token));
  }

}