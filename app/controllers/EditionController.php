<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use App\Models\Article;

class EditionController
{
  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function index(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $editions = Article::editions();

    return $this->container->view->render($res, "/editions/index.html.twig", [
      'token' => $token,
      'editions' => $editions
    ]);
  }

  public function edition(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
   $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $edition = Article::edition($req->getAttribute('number'));

    if($edition) {

      return $this->container->view->render($res, "/editions/single.html.twig", [
        'token' => $token,
        'edition' => $edition
      ]);

    }

    return $res->withStatus(404)->withHeader('Location', '/error');

  }

}