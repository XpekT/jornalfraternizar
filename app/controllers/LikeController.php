<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use App\Models\Like;

class LikeController 
{

  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function likeArticle(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $like = new Like;

    return $res->withJson($like->likeArticle($req->getParsedBody(), $token));
  }

  public function likeBook(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $like = new Like;

    return $res->withJson($like->likeBook($req->getParsedBody(), $token));
  }

}