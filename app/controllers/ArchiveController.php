<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;

class ArchiveController
{
  protected $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function index(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    return $this->container->view->render($res, "archive.html.twig", [
      'token' => $token
    ]);
  }

  public function item(ServerRequestInterface $req, ResponseInterface $res, $args)
  {
    $nameKey = $this->container->csrf->getTokenNameKey();
    $valueKey = $this->container->csrf->getTokenValueKey();

    $token = [
      'csrf_name' => $req->getAttribute($nameKey),
      'csrf_value' => $req->getAttribute($valueKey)
    ];

    $year = $req->getAttribute('year');
    $number = $req->getAttribute('number');

    $file = $this->container->fileLoader->render("/archive/{$year}/{$number}.html");

    return $res->withJson([
      "token" => $token,
      "file" => $file
    ]);
  }

}