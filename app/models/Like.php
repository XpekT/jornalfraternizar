<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Classes\Helpers;
use Valitron\Validator;

class like extends Eloquent
{
  protected $table = "likes";
  protected $fillable = ["rating", "ip", "likeable_id", "likeable_type"];

  // Relationships
  public function likeable()
  {
    return $this->morphTo();
  }

  public function likeArticle($data, $token)
  {
    $ip = Helpers::getIp();
    $articleId = Helpers::sanitize($data['id']);
    $rating = Helpers::sanitize($data['rating']);

    if($ip != 'UNKNOWN') {

      // Check if IP has already liked the article
      $like = $this->where("likeable_type", "App\Models\Article")->where('likeable_id', $articleId)->where('ip', $ip)->get()->first();

      if($like) {

        return [
          'errors' => [Helpers::message('error', 'Já gostou deste artigo!')],
          'token' => $token
        ]; 

      } else {

        $this->ip = $ip;
        $this->rating = $rating;
        $this->likeable_id = $articleId;
        $this->likeable_type = "App\Models\Article";

        if($this->save()) {

          return [
            "message" => 'Obrigado pelo seu feedback!',
            "like" => $this,
            "token" => $token
          ];

        } else {

          return [
            'errors' => [Helpers::message('error')],
            'token' => $token
          ];

        }

      }

    } else {

      return [
        'errors' => [Helpers::message('error')],
        'token' => $token
      ];

    }
    
  }

  public function likeBook($data, $token)
  {
    $ip = Helpers::getIp();
    $bookId = Helpers::sanitize($data['id']);
    $rating = Helpers::sanitize($data['rating']);

    if($ip != 'UNKNOWN') {

      // Check if IP has already liked the article
      $like = $this->where("likeable_type", "App\Models\Book")->where('likeable_id', $bookId)->where('ip', $ip)->get()->first();

      if($like) {

        return [
          'errors' => [Helpers::message('error', 'Já gostou deste livro!')],
          'token' => $token
        ]; 

      } else {

        $this->ip = $ip;
        $this->rating = $rating;
        $this->likeable_id = $bookId;
        $this->likeable_type = "App\Models\Book";

        if($this->save()) {

          return [
            "message" => 'Obrigado pelo seu feedback!',
            "like" => $this,
            "token" => $token
          ];

        } else {

          return [
            'errors' => [Helpers::message('error')],
            'token' => $token
          ];

        }

      }

    } else {

      return [
        'errors' => [Helpers::message('error')],
        'token' => $token
      ];

    }
    
  }

}