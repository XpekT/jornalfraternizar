<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Classes\Helpers;
use App\Classes\Session;
use Valitron\Validator;
use Cocur\Slugify\Slugify;
use App\Models\User;

class Article extends Eloquent
{
  protected $table = "articles";
  protected $fillable = ["title", "edition", "text", "category", "slug", "user_id", "section", "author"];

  protected $categories = ["Igreja", "Fátima", "Poemas", "Fé", "Entrevistas", "Livros", "Política", "Opinião"];
  protected $sections = ["main", "collab"];

  // Relationships
  public function user()
  {
    return $this->belongsTo('\App\Models\User');
  }

  public function comments()
  {
    return $this->morphMany('App\Models\Comment', 'commentable');
  }

  public function likes()
  {
    return $this->morphMany('App\Models\Like', 'likeable');
  }

  // Get editions
  public static function editions()
  {
    $editions = self::orderBy('edition', 'desc')->get()->map(function($edition) {
      return $edition->edition;
    });
    
    return $editions->unique();
  }

  public static function edition($number)
  {
    $edition = self::where('edition', $number)->orderBy('created_at', 'desc')->with('user')
                   ->with('comments')->with('likes')->get()->all();

    return $edition;
  }

  // Get Single Article by slug
  public static function single($slug = '')
  {
    return self::where('slug', $slug)->with('user')->with('comments')->with('likes')->get()->first();
  }

  public function create_article($data, $token)
  {
    $title = Helpers::sanitize($data['title']);
    $category = Helpers::sanitize($data['category']);
    $edition = Helpers::sanitize($data['edition']);
    $text = Helpers::sanitize($data['text']);
    $section = Helpers::sanitize($data['section']);
    $author = Helpers::sanitize($data['author']);
    
    $v = new Validator($data);

    // Validator Rules
    $v->rule('required', ['title', 'category', 'edition', 'text', 'section']);
    $v->rule('integer', 'edition');
    $v->rule('in', $this->categories, 'category');
    $v->rule('in', $this->sections, 'section');
    $v->rule('lengthMin', 'title', 3);
    $v->rule('lengthMin', 'text', 10);

    if($v->validate()) {

      $slugifier = new Slugify();

      $this->title = $title;
      $this->category = $category;
      $this->edition = $edition;
      $this->text = $text;
      $this->slug = $slugifier->slugify(mb_strtolower($title));
      $this->user_id = Session::get('userSession')['id'];
      $this->section = $section;
      $this->author = Helpers::isEmpty($author) ? Session::get('userSession')['username'] : $author;
 
      if($this->save()) {

        // Update user session
        $user = User::find(Session::get('userSession')['id']);

        $userSessionData = [
          'id' => $user->id,
          'username' => $user->username,
          'email' => $user->email,
          'role' => $user->role,
          'articles' => $user->articles
        ];

        Session::set('userSession', $userSessionData);

        return [
          "message" => "Artigo Criado!",
          "token" => $token
        ];

      } else {

        return [
          "errors" => [Helpers::message('error')],
          "token" => $token
        ];

      }  

    } else {
    
      return [
        "errors" => [Helpers::message('error', 'Formulário não está devidamente preenchido.')],
        "token" => $token
      ];
    
    }
  }

  public function delete_article($data, $token)
  {

    $id = Helpers::sanitize($data['id']);

    $article = $this->where('id', $id)->get()->first();
    $user = Session::get('userSession');

    if($article && $article->user_id == $user['id'] || $user['role'] == 'Administrador') {

      if($article->delete()) {
        
        return [
          'status' => 200,
          'token' => $token
        ];

      }

    }

    return [
      "errors" => [Helpers::message('error')],
      "token" => $token
    ];
  }

  public function edit_article($data, $token)
  {
    $user = Session::get('userSession');
    $articleId = Helpers::sanitize($data['id']);

    // Check if article exists and belongs to logged in user
    $article = $this->where('id', $articleId)->get()->first();

    if($article && $article->user_id == $user['id'] || $user['role'] == 'Administrador') {

      $v = new Validator($data);
      $slug = NULL;

      if(!Helpers::isEmpty($data['title'])) {
        $v->rule('required', 'title');
        $v->rule('lengthMin', 'title', 3);
        
        $slugifier = new Slugify();
        
        $article->title = Helpers::sanitize($data['title']);
        $article->slug = $slugifier->slugify(Helpers::sanitize($data['title']));

        $field = 'title';
        $value = $article->title;
        $slug = $article->slug;

      }

      if(!Helpers::isEmpty($data['edition'])) {
        $v->rule('required', 'edition');
        $v->rule('integer', 'edition');

        $article->edition = Helpers::sanitize($data['edition']);

        $field = 'edition';
        $value = $article->edition;
      }

      if(!Helpers::isEmpty($data['author'])) {

        $v->rule('required', 'author');
        $v->rule('lengthMin', 'author', 3);

        $article->author = Helpers::sanitize($data['author']);

        $field = 'author';
        $value = $article->author;
      }

      if(!Helpers::isEmpty($data['section'])) {
        $v->rule('required', 'section');
        $v->rule('in', $this->sections, 'section');

        $article->section = Helpers::sanitize($data['section']);

        $field = 'section';
        $value = $article->section;
      }

      if(!Helpers::isEmpty($data['category'])) {
        $v->rule('required', 'category');
        $v->rule('in', $this->categories, 'category');

        $article->category = Helpers::sanitize($data['category']);

        $field = 'category';
        $value = $article->category;
      }

      if(!Helpers::isEmpty($data['text'])) {
        $v->rule('required', 'text');
        $v->rule('lengthMin', 'text', 10);

        $article->text = Helpers::sanitize($data['text']);

        $field = 'text';
        $value = $article->text;
      }

      if($v->validate()) {

        if($article->save()) {

          if($slug != NULL) {
            
            return [
              "message" => "Artigo Editado!",
              "token" => $token,
              "field" => $field,
              "value" => $value,
              "id" => $articleId,
              "slug" => $slug
            ];
        
          } else {

            return [
              "message" => "Artigo Editado!",
              "token" => $token,
              "field" => $field,
              "value" => $value,
              "id" => $articleId,
              "slug" => $article->slug
            ];

          }
        
        }

        return [
          "errors" => [Helpers::message('error')],
          "token" => $token
        ];

      }
 
      return [
        "errors" => [Helpers::message('error', 'Formulário não está devidamente preenchido.')],
        "token" => $token
      ];

    }

    return [
      "errors" => [Helpers::message('error')],
      "token" => $token
    ];
  }

}