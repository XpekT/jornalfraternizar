<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Classes\Helpers;
use App\Classes\Session;
use Valitron\Validator;
use Cocur\Slugify\Slugify;

class Book extends Eloquent
{
  protected $table = "books";
  protected $fillable = ["title", "author", "summary", "image", "price", "buy", "user_id"];

  // Relationships
  public function user()
  {
    return $this->belongsTo('App\Models\User');
  }

  public function comments()
  {
    return $this->morphMany('App\Models\Comment', 'commentable');
  }

  public function likes()
  {
    return $this->morphMany('App\Models\Like', 'likeable');
  }

  public static function single($slug = '')
  {
    return self::where('slug', $slug)->with('user')->with('comments')->with('likes')->get()->first();
  }

  public function create_book($data, $token)
  {

    $data = Helpers::sanitize($data);

    // Validate Data
    $v = new Validator($data);

    // Validator Rules
    $v->rule('required', ['title', 'author', 'image', 'summary', 'price', 'buy']);
    $v->rule('numeric', 'price');
    $v->rule('url', ['buy']);
    $v->rule('lengthMin', 'title', 3);
    $v->rule('lengthMin', 'summary', 10);
    $v->rule('lengthMax', 'summary', 2000);

    if($v->validate()) {

      $slugifier = new Slugify;

      $this->title = $data['title'];
      $this->author = $data['author'];
      $this->image = '/assets/images/books/' . $data['image'];
      $this->summary = $data['summary'];
      $this->price = $data['price'];
      $this->buy = $data['buy'];
      $this->slug = $slugifier->slugify($data['title']);
      $this->user_id = Session::get('userSession')['id'];

      if($this->save()) {

        return [
          "message" => "Livro Adicionado!",
          "token" => $token
        ];

      } else {

         return [
          "errors" => [Helpers::message('error')],
          "token" => $token
        ];

      }
      

    } else {

      return [
        "errors" => [Helpers::message('error', 'Formulário não está devidamente preenchido.')],
        "token" => $token
      ];

    }

  }

  public function edit_book($data, $token)
  {
    $user = Session::get('userSession');
    $bookId = Helpers::sanitize($data['id']);

    // Check if book exists and belongs to logged in user
    $book = $this->where('id', $bookId)->get()->first();

    if($book && $book->user_id == $user['id'] || $user['role'] == 'Administrador') {

      $v = new Validator($data);
      $slug = NULL;

      if(!Helpers::isEmpty($data['title'])) {
        
        $v->rule('required', 'title');
        $v->rule('lengthMin', 'title', 3);
        
        $slugifier = new Slugify();
        
        $book->title = Helpers::sanitize($data['title']);
        $book->slug = $slugifier->slugify(Helpers::sanitize($data['title']));

        $field = 'title';
        $value = $book->title;
        $slug = $book->slug;

      }

      if(!Helpers::isEmpty($data['author'])) {
        $v->rule('required', 'author');

        $book->author = Helpers::sanitize($data['author']);

        $field = 'author';
        $value = $book->author;
      }

      if(!Helpers::isEmpty($data['image'])) {
        $v->rule('required', 'image');
        $v->rule('url', 'image');

        $book->image = Helpers::sanitize($data['image']);

        $field = 'image';
        $value = $book->image;
      }

      if(!Helpers::isEmpty($data['summary'])) {
        $v->rule('required', 'summary');
        $v->rule('lengthMin', 'summary', 10);
        $v->rule('lengthMax', 'summary', 1000);

        $book->summary = Helpers::sanitize($data['summary']);

        $field = 'summary';
        $value = $book->summary;
      }

      if(!Helpers::isEmpty($data['price'])) {
        $v->rule('required', 'price');
        $v->rule('numeric', 'price');
        
        $book->price = Helpers::sanitize($data['price']);

        $field = 'price';
        $value = $book->price;
      }

      if(!Helpers::isEmpty($data['buy'])) {
        $v->rule('required', 'buy');
        $v->rule('url', 'buy');

        $book->buy = Helpers::sanitize($data['buy']);

        $field = 'buy';
        $value = $book->buy;
      }

      if($v->validate()) {

        if($book->save()) {

          if($slug != NULL) {

            return [
              "message" => "Livro Editado!",
              "token" => $token,
              "field" => $field,
              "value" => $value,
              "id" => $bookId,
              "slug" => $slug
            ];
          
          } else {

            return [
              "message" => "Livro Editado!",
              "token" => $token,
              "field" => $field,
              "value" => $value,
              "id" => $bookId,
              "slug" => $book->slug
            ];

          }


        }

        return [
          "errors" => [Helpers::message('error')],
          "token" => $token
        ];

      }
 
      return [
        "errors" => [Helpers::message('error', 'Formulário não está devidamente preenchido.')],
        "token" => $token
      ];

    }

    return [
      "errors" => [Helpers::message('error')],
      "token" => $token
    ];
  }

  public function delete_book($data, $token)
  {

    $id = Helpers::sanitize($data['id']);

    $book = $this->where('id', $id)->get()->first();
    $user = Session::get('userSession');

    if($book && $book->user_id == $user['id'] || $user['role'] == 'Administrador') {

      if($book->delete()) {
        
        return [
          'status' => 200,
          'token' => $token
        ];

      }

    }

    return [
      "errors" => [Helpers::message('error')],
      "token" => $token
    ];
  }

}