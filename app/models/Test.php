<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Test extends Eloquent
{
  protected $table = "test";
}