<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Valitron\Validator;
use App\Classes\Helpers;
use Hautelook\Phpass\PasswordHash;
use App\Classes\Mail;
use App\Classes\Session;
use App\Models\Article;
use App\Models\Book;
use App\Models\Video;
use Carbon\Carbon;

class User extends Eloquent
{
  protected $table = "users";
  protected $fillable = ["usernameOrEmail", "password", "last_session_at"];
  protected $hidden = ["password"];

  protected $roles = ['Administrador', 'Colaborador'];

  // Relationships
  public function articles()
  {
    return $this->hasMany('\App\Models\Article');
  }

  public function books()
  {
    return $this->hasMany('\App\Models\Book');
  }

  public function videos()
  {
    return $this->hasMany('\App\Models\Video');
  }

  public function get_collectibles($data, $token)
  {
    $id = Helpers::sanitize($data['userId']);
    
    $articles = Article::where('user_id', $id)->with("comments")->with("likes")->get()->all();
    $books = Book::where('user_id', $id)->with("comments")->with("likes")->get()->all();
    $videos = Video::where('user_id', $id)->get()->all();

    return [
      'articles' => $articles,
      'books' => $books,
      'videos' => $videos,
      'token' => $token
    ];
  }

  public function create_user($data, $token)
  {
    $email = Helpers::sanitize($data['email']);
    $username = Helpers::sanitize($data['username']);

    $user = $this->where('email', $email)->orWhere('username', $username)->first();

    // Check if user has not activated his account
    if($user && $user->active == 0) {

      $mail = new Mail();

      if($mail->activate($user->email, $user->username, $user->code)) {

        return [
          "errors" =>[ Helpers::message('error', 'A sua conta ainda não se encontra activada! Verifique o seu email.')],
          "token" => $token
        ];

      }

      return [
        "errors" => Helpers::message('error')
      ];
    
    }

    // Check if user already exists
    if($user) {
      return [
        "errors" => [Helpers::message('error', 'Nome de Utilizador ou Email já em uso.')],
        "token" => $token
      ];
    }

    // Variables
    $v = new Validator($data);
    $hash = new PasswordHash(12, false);

    // Validator Rules
    $v->rule('required', ['username', 'email']);
    $v->rule('email', 'email');
    $v->rule('in', $this->roles, 'role');

    // Validate Input
    if($v->validate()) {
      
      $password = bin2hex(openssl_random_pseudo_bytes(8));

      // Create New User
      $this->username = $username;
      $this->email = $email;
      $this->password = $hash->hashPassword($password);
      $this->role = Helpers::sanitize($data['role']);
      $this->code = md5(microtime());

      // Mail activate code
      $mail = new Mail();

      if($this->save() && $mail->activate($this->email, $this->username, $this->code, $password)) {

        return [
          "message" => Helpers::message('success', 'Utilizador Criado!')
        ];

      } 

      return [
        "errors" => [Helpers::message('error')],
        "token" => $token
      ];
    
    } else {
    
      return [
        "errors" => [Helpers::message('error', 'Formulário não está devidamente preenchido.')],
        "token" => $token
      ];
    
    }
  }

  public function activate_user($data)
  {

    // Get data and sanitize
    $email = Helpers::sanitize($data["email"]);
    $code = Helpers::sanitize($data["code"]);

    $user = $this->where('email', $email)->where('code', $code)->first();

    if($user->active === 0) {

      $user->active = 1;
      $user->save();

      return true;

    }

    return false;

  }

  public function login_user($data, $token)
  {
    $usernameOrEmail = $data['usernameOrEmail'];
    $password = Helpers::sanitize($data['password']);

    $v = new Validator($data);
    $passwordHasher = new PasswordHash(12, false);

    // Validator Rules
    $v->rule('required', ['usernameOrEmail', 'password']);

    // Validate Input
    if($v->validate()) {

      // Check if user exists
      $user = $this->where('username', $usernameOrEmail)->orWhere('email', $usernameOrEmail)->get()->first();

      if($user && $user->active == 1) {

        // Check password
        $passwordMatch = $passwordHasher->CheckPassword($password, $user->password);

        if($passwordMatch) {

          $userSessionData = [
            'id' => $user->id,
            'username' => $user->username,
            'email' => $user->email,
            'role' => $user->role,
            'last_session_at' => $user->last_session_at
          ];

          // If Passwords match - Login user
          Session::set('userSession', $userSessionData);

          $user->last_session_at = Carbon::now();

          $user->save();

          return [
            "status" => 200,
            "user" => $userSessionData,
            "redirect" => true
          ];

        }

        return [
          "errors" => [Helpers::message('error', 'Credenciais Erradas!')],
          "token" => $token
        ];  

      }

      return [
        "errors" => [Helpers::message('error', 'Utilizador Não Existe Ou Conta Por Activar!')],
        "token" => $token
      ];

    } else {

      return [
        "errors" => $v->errors(),
        "token" => $token
      ];

    }

  }

  public static function logout_user()
  {
    return Session::clear('userSession');
  }

  public function recover_password($data, $token)
  {

    $usernameOrEmail = Helpers::sanitize($data['usernameOrEmail']);

    // Get user from DB
    $user = $this->where("username", $usernameOrEmail)->orWhere("email", $usernameOrEmail)->get()->first();

    if($user && $user->active == 1) {

      // Reset password and send mail
      $newPassword = bin2hex(random_bytes(8));

      // Mail activate code
      $mail = new Mail();

      $hash = new PasswordHash(12, false);
      $user->password = $hash->hashPassword($newPassword);

      if($user->save() && $mail->recover($user->email, $user->username, $newPassword)) {

        return [
          "token" => $token
        ];

      } else {

        return [
        "errors" => [Helpers::message('error')],
        "token" => $token
      ];

      }  

    } else {

      return [
        "errors" => [Helpers::message('error', 'Utilizador Não Existe Ou Conta Por Activar!')],
        "token" => $token
      ];

    }

  }

  public function edit_profile($data, $token)
  {

    // Get user
    $user = $this->where('username', Session::get('userSession')['username'])->get()->first();

    if(!Helpers::isEmpty($data['username'])) {

      $username = Helpers::sanitize($data['username']);

      $user->username = $username;

      if($user->save()) {

        // Set new user data in session
        Session::set('userSession', $user);

        return [
          'status' => 200,
          'message' => 'Nome de Utilizador Actualizado!',
          'token' => $token,
          'user' => $user
        ];

      } else {

        return [
          'errors' => [Helpers::message('error')]
        ];

      }

    }

    if(!Helpers::isEmpty($data['email'])) {
      
      $email = Helpers::sanitize($data['email']);

      $user->email = $email;

      if($user->save()) {

        // Set new user data in session
        Session::set('userSession', $user);

        return [
          'status' => 200,
          'message' => 'Email Actualizado!',
          'token' => $token,
          'user' => $user
        ];

      } else {

        return [
          'errors' => [Helpers::message('error')]
        ];

      }
    }

    if(!Helpers::isEmpty($data['password'])) {
      
      $hash = new PasswordHash(12, false);

      $password = Helpers::sanitize($data['password']);

      $user->password = $hash->hashPassword($password);

      if($user->save()) {

        // Set new user data in session
        Session::set('userSession', $user);

        return [
          'status' => 200,
          'message' => 'Palavra-Passe Actualizada!',
          'token' => $token,
          'user' => $user
        ];

      } else {

        return [
          'errors' => [Helpers::message('error')],
          'token' => $token
        ];

      }

    }

    return [
      'errors' => [Helpers::message('error')],
      'token' => $token
    ];

  }

  public function delete_profile($data, $token)
  {
    $user = $this->where('id', Helpers::sanitize($data['id']))->get()->first();

    if($user) {

      if($user->delete()) {

        if($data['own']) {
          Session::clear('userSession');
        }

        return [
          'deleted' => true,
          'token' => $token
        ];

      } else {

        return [
          'errors' => [Helpers::message('error')],
          'token' => $token
        ];

      }

    } else {

      return [
        'errors' => [Helpers::message('error')],
        'token' => $token
      ];

    }

  }

}