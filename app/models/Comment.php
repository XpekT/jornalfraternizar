<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Classes\Helpers;
use Valitron\Validator;

class Comment extends Eloquent
{
  protected $table = "comments";
  protected $fillable = ["name", "email", "text", "commentable_id", "commentable_type"];

  // Relationships
  public function commentable()
  {
    return $this->morphTo();
  }

  public function article_add($data, $token)
  {

    $sanitized = Helpers::sanitize($data);

    $v = new Validator($sanitized);

    $v->rule('required', ['name', 'email', 'text']);
    $v->rule('email', 'email');
    $v->rule('lengthMin', 'name', 3);
    $v->rule('lengthMin', 'text', 10);

    if($v->validate()) {

      $this->name = $sanitized['name'];
      $this->email = $sanitized['email'];
      $this->text = $sanitized['text'];
      $this->commentable_id = $sanitized['article_id'];
      $this->commentable_type = 'App\Models\Article';

      if($this->save()) {
      
        // Get the comment to update the state
        $comment = $this;
        
        return [
          'comment' => $comment,
          'message' => 'Comentário Adicionado!',
          'token' => $token
        ];

      } else {

        return [
          'errors' => [Helpers::message('error')],
          'token' => $token
        ];

      }


    } else {

      return [
        'errors' => [Helpers::message('error', 'Formulário não está devidamente preenchido.')],
        'token' => $token
      ];
    
    }

  }

  public function article_remove($data, $token)
  {
    $id = Helpers::sanitize($data['id']);

    $comment = self::find($id);

    if($comment && $comment->delete()) {

      return [
        'message' => 'Comentário Eliminado!',
        'token' => $token
      ];
    
    } else {

      return [
        'errors' => [Helpers::message('error')],
        'token' => $token
      ];

    } 

  }

  public function book_add($data, $token)
  {

    $sanitized = Helpers::sanitize($data);

    $v = new Validator($sanitized);

    $v->rule('required', ['name', 'email', 'text']);
    $v->rule('email', 'email');
    $v->rule('lengthMin', 'name', 3);
    $v->rule('lengthMin', 'text', 10);

    if($v->validate()) {

      $this->name = $sanitized['name'];
      $this->email = $sanitized['email'];
      $this->text = $sanitized['text'];
      $this->commentable_id = $sanitized['book_id'];
      $this->commentable_type = 'App\Models\Book';

      if($this->save()) {
      
        // Get the comment to update the state
        $comment = $this;
        
        return [
          'comment' => $comment,
          'message' => 'Comentário Adicionado!',
          'token' => $token
        ];

      } else {

        return [
          'errors' => [Helpers::message('error')],
          'token' => $token
        ];

      }


    } else {

      return [
        'errors' => [Helpers::message('error', 'Formulário não está devidamente preenchido.')],
        'token' => $token
      ];
    
    }

  }

  public function book_remove($data, $token)
  {
    $id = Helpers::sanitize($data['id']);

    $comment = self::find($id);

    if($comment && $comment->delete()) {

      return [
        'message' => 'Comentário Eliminado!',
        'token' => $token
      ];
    
    } else {

      return [
        'errors' => [Helpers::message('error')],
        'token' => $token
      ];

    } 

  }

}