<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Classes\Helpers;
use App\Classes\Session;
use Valitron\Validator;
use Cocur\Slugify\Slugify;

class Video extends Eloquent
{
  protected $table = "videos";
  protected $fillable = ["title", "link", "user_id"];

  // Relationships
  public function user()
  {
    return $this->belongsTo('App\Models\User');
  }

  public function create_video($data, $token)
  {

    $data = Helpers::sanitize($data);

    // Validate Data
    $v = new Validator($data);

    // Validator Rules
    $v->rule('required', ['title', 'link']);
    $v->rule('lengthMin', 'title', 3);

    if($v->validate()) {

      $slugifier = new Slugify;

      $this->title = $data['title'];
      $this->link = 	"https://www.youtube.com/embed/" . $data['link'];
      $this->slug = $slugifier->slugify($data['title']);
      $this->user_id = Session::get('userSession')['id'];

      if($this->save()) {

        return [
          "message" => "Video Adicionado!",
          "token" => $token
        ];

      } else {

         return [
          "errors" => [Helpers::message('error')],
          "token" => $token
        ];

      }
      

    } else {

      return [
        "errors" => [Helpers::message('error', 'Formulário não está devidamente preenchido.')],
        "token" => $token
      ];

    }

  }

  public function edit_video($data, $token)
  {
    $user = Session::get('userSession');
    $videoId = Helpers::sanitize($data['id']);

    // Check if video exists and belongs to logged in user
    $video = $this->where('id', $videoId)->get()->first();

    if($video && $video->user_id == $user['id'] || $user['role'] == 'Administrador') {

      $v = new Validator($data);
      $slug = NULL;

      if(!Helpers::isEmpty($data['title'])) {
        
        $v->rule('required', 'title');
        $v->rule('lengthMin', 'title', 3);
        
        $slugifier = new Slugify();
        
        $video->title = Helpers::sanitize($data['title']);
        $video->slug = $slugifier->slugify(Helpers::sanitize($data['title']));

        $field = 'title';
        $value = $video->title;
        $slug = $video->slug;

      }

      if(!Helpers::isEmpty($data['link'])) {
        $v->rule('required', 'link');

        $video->link = 	'https://www.youtube.com/embed/' . Helpers::sanitize($data['link']);

        $field = 'link';
        $value = $video->link;
      }

      if($v->validate()) {

        if($video->save()) {

          if($slug != NULL) {

            return [
              "message" => "Video Editado!",
              "token" => $token,
              "field" => $field,
              "value" => $value,
              "id" => $videoId,
              "slug" => $slug
            ];

          } else {

             return [
              "message" => "Video Editado!",
              "token" => $token,
              "field" => $field,
              "value" => $value,
              "id" => $videoId,
              "slug" => $video->slug
            ];

          }

        }

        return [
          "errors" => [Helpers::message('error')],
          "token" => $token
        ];

      }
 
      return [
        "errors" => [Helpers::message('error', 'Formulário não está devidamente preenchido.')],
        "token" => $token
      ];

    }

    return [
      "errors" => [Helpers::message('error')],
      "token" => $token
    ];
  }

  public function delete_video($data, $token)
  {

    $id = Helpers::sanitize($data['id']);

    $video = $this->where('id', $id)->get()->first();
    $user = Session::get('userSession');

    if($video && $video->user_id == $user['id'] || $user['role'] == 'Administrador') {

      if($video->delete()) {
        
        return [
          'status' => 200,
          'token' => $token
        ];

      }

    }

    return [
      "errors" => [Helpers::message('error')],
      "token" => $token
    ];
  }

}