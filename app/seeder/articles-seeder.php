<?php

require "vendor/autoload.php";

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Classes\Database;
use Faker\Factory as Faker;
use Cocur\Slugify\Slugify;
use App\Models\Article;

$db = new Database([
  "driver" => "mysql",
  "host" => "127.0.0.1",
  "database" => "jornalfraternizar",
  "username" => "root",
  "password" => "root",
  "charset" => "utf8",
  "collation" => "utf8_unicode_ci",
  "prefix" => ""
]);

$db->start();

$faker = Faker::create();
$slugifier = new Slugify;

for($i = 0; $i < 50; $i++)
{

  $title = $faker->sentence;
  $slug = $slugifier->slugify($title);

  Article::create([
    'title' => $title,
    'category' => $faker->randomElement(["Igreja", "Fátima", "Poemas", "Fé", "Entrevistas"]),
    'edition' => $faker->numberBetween(1, 150),
    'text' => $faker->realText(5000, $indexSize = 2),
    'slug' => $slug,
    'user_id' => $faker->randomElement([1, 2])
  ]);
}

return "OK!";

