<?php

require "vendor/autoload.php";

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Classes\Database;
use Faker\Factory as Faker;
use App\Models\Like;

$db = new Database([
  "driver" => "mysql",
  "host" => "127.0.0.1",
  "database" => "jornalfraternizar",
  "username" => "root",
  "password" => "root",
  "charset" => "utf8",
  "collation" => "utf8_unicode_ci",
  "prefix" => ""
]);

$db->start();

$faker = Faker::create();

for($i = 0; $i < 50; $i++)
{

  Like::create([
    'rating' => $faker->numberBetween(1, 5),
    'ip' => $faker->ipv4,
    'likeable_id' => $faker->numberBetween(1, 150),
    'likeable_type' => $faker->randomElement(['App\Models\Article', 'App\Models\Book'])
  ]);
}

echo "OK!";

return;