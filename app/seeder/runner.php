<?php

require "vendor/autoload.php";

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Classes\Database;
use App\Models\Article;

$db = new Database([
  "driver" => "mysql",
  "host" => "127.0.0.1",
  "database" => "jornalfraternizar",
  "username" => "root",
  "password" => "root",
  "charset" => "utf8",
  "collation" => "utf8_unicode_ci",
  "prefix" => ""
]);

$db->start();

$count = Article::count();

for($i = 1; $i < ($count + 1); $i++)
{
  $article = Article::find($i);
  $article->author = 'Padre Mário';
  $article->save();
}

echo "DONE!";

