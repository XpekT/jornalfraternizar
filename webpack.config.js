var webpack = require("webpack")
var path = require("path")
var ExtractTextPlugin = require("extract-text-webpack-plugin")

var extractLess = new ExtractTextPlugin({
  filename: "styles/main.min.css"
})

module.exports = {
  entry: ["./src/js/app.js", "./src/styles/main.less"],
  output: {
    path: path.resolve(__dirname, './app/public/assets'),
    filename: 'js/bundle.min.js',
    publicPath: './app/public'
  },
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        exclude: '/node_modules/',
        use: extractLess.extract({
          use: [
            {
              loader: "css-loader",
              options: {
                minimize: true
              }
            }
          ]
        })
      },
      {
        test: /\.less$/,
        exclude: '/node_modules/',
        use: extractLess.extract({
          use: [
            {
              loader: "css-loader",
              options: {
                minimize: true
              }
            }, 
            {
              loader: "less-loader"
            },
            {
              loader: "postcss-loader"
            },
          ],
          fallback: "style-loader"
        })
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: '/node_modules/'
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
          }
        }
      }
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
    extractLess,
    // new webpack.DefinePlugin({
    //   'process.env': {
    //     NODE_ENV: '"production"'
    //   }
    // })
  ]
}