export default {
  notification: null,
  page: '',
  user: null,
  loading: true,
  token: {
    name: null,
    value: null
  },
  modal: {
    active: false,
    data: {},
    type: ''
  },
  article: {},
  book: {},
  video: {},
  articles: [],
  books: [],
  videos: [],
  search: {
    active: false,
    results: {
      articles: [],
      books: [],
      videos: []
    },
    lastQuery: ''
  },
  stats: {
    users: [],
    articles: 0,
    books: 0,
    videos: 0
  },
  visitors: '<span class="glyphicon glyphicon-hourglass"></span>'
}