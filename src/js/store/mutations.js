export default {
  changePage(state, payload) {
    return state.page = payload
  },
  addNotification(state, payload) {

    if(payload.autoClear === true) {
      setTimeout(() => {
        return state.notification = null
      }, 5000)
    }

    return state.notification = payload
  },
  removeNotification(state) {
    return state.notification = null
  },
  changeUser(state, payload) {
    return state.user = payload
  },
  changeLoading(state, payload) {
    return state.loading = payload
  },
  changeToken(state, payload) {
    return state.token = payload
  },
  changeModal(state, payload) {
    state.modal.active = payload.active
    state.modal.data = payload.data
    return state.modal.type = payload.type
  },
  currentArticle(state, payload) {
    return state.article = payload
  },
  updateArticle(state, payload) {

    if(payload.field == 'title') {

      if(state.articles.length != 0) {

        let index = state.articles.findIndex(article => {
          return article.id == payload.id
        })

        state.articles[index].title = payload.value
        state.articles[index].slug = payload.slug

      } 

      return state.article.title = payload.value
    }

    if(payload.field == 'edition') {

      if(state.articles.length != 0) {

        let index = state.articles.findIndex(article => {
          return article.id == payload.id
        })

        state.articles[index].edition = payload.value
      
      }

      return state.article.edition = payload.value
    }

    if(payload.field == 'category') {

      if(state.articles.length != 0) {

        let index = state.articles.findIndex(article => {
          return article.id == payload.id
        })

        state.articles[index].category = payload.value

      }  

      return state.article.category = payload.value
    }

    if(payload.field == 'text') {

      if(state.articles.length != 0) {

        let index = state.articles.findIndex(article => {
          return article.id == payload.id
        })

        state.articles[index].text = payload.value

      }  

      return state.article.text = payload.value
    }

  },
  loadArticles(state, payload) {
    return state.articles = payload
  },
  loadBooks(state, payload) {
    return state.books = payload
  },
  loadVideos(state, payload) {
    return state.videos = payload
  },
  deleteArticle(state, payload) {

    let index = state.articles.findIndex(article => {
      return article.id == payload.id
    })

    return state.articles.splice(index, 1)

  },
  addArticleComment(state, payload) {
    return state.article.comments.push(payload)
  },
  addBookComment(state, payload) {
    return state.book.comments.push(payload)
  },
  removeBookComment(state, payload) {
    
    let index = state.book.comments.findIndex(comment => {
      return comment.id == payload.id
    })

    return state.book.comments.splice(index, 1)

  },
  removeArticleComment(state, payload) {
    
    let index = state.article.comments.findIndex(comment => {
      return comment.id == payload.id
    })

    return state.article.comments.splice(index, 1)

  },
  addBookLike(state, payload) {
    return state.book.likes.push(payload)
  },
  addArticleLike(state, payload) {
    return state.article.likes.push(payload)
  },
  openSearch(state) {
    return state.search.active = true
  },
  updateSearch(state, payload) {

      state.search.results.articles = payload.results.articles
      state.search.results.books = payload.results.books
      state.search.results.videos = payload.results.videos
      
      state.search.lastQuery = payload.query
      
      return state.search.active = true
  },
  closeSearch(state, payload) {
    return state.search.active = false
  },
  showSearch(state, payload) {
    return state.search.active = true
  },
  currentBook(state, payload) {
    return state.book = payload
  },
  updateBook(state, payload) {

    if(payload.field == 'title') {

      if(state.books.length != 0) {

        let index = state.books.findIndex(book => {
          return book.id == payload.id
        })

        state.books[index].title = payload.value
        state.books[index].slug = payload.slug

      } 

      return state.book.title = payload.value
    }

    if(payload.field == 'author') {

      if(state.books.length != 0) {

        let index = state.books.findIndex(book => {
          return book.id == payload.id
        })

        state.books[index].author = payload.value
        state.books[index].slug = payload.slug

      } 

      return state.book.author = payload.value
    }

    if(payload.field == 'image') {

      if(state.books.length != 0) {

        let index = state.books.findIndex(book => {
          return book.id == payload.id
        })

        state.books[index].image = payload.value
        state.books[index].slug = payload.slug

      } 

      return state.book.image = payload.value
    }

    if(payload.field == 'summary') {

      if(state.books.length != 0) {

        let index = state.books.findIndex(book => {
          return book.id == payload.id
        })

        state.books[index].summary = payload.value
        state.books[index].slug = payload.slug

      } 

      return state.book.summary = payload.value
    }

    if(payload.field == 'price') {

      if(state.books.length != 0) {

        let index = state.books.findIndex(book => {
          return book.id == payload.id
        })

        state.books[index].price = payload.value
        state.books[index].slug = payload.slug

      } 

      return state.book.price = payload.value
    }

    if(payload.field == 'buy') {

      if(state.books.length != 0) {

        let index = state.books.findIndex(book => {
          return book.id == payload.id
        })

        state.books[index].buy = payload.value
        state.books[index].slug = payload.slug

      } 

      return state.book.buy = payload.value
    }

  },
  updateVideo(state, payload) {

    if(payload.field == 'title') {

      if(state.videos.length != 0) {

        let index = state.videos.findIndex(video => {
          return video.id == payload.id
        })

        state.videos[index].title = payload.value
        state.videos[index].slug = payload.slug

      } 

      return state.video.title = payload.value
    }

    if(payload.field == 'link') {

      if(state.videos.length != 0) {

        let index = state.videos.findIndex(video => {
          return video.id == payload.id
        })

        state.videos[index].link = payload.value
        state.videos[index].slug = payload.slug

      } 

      return state.video.link = payload.value
    }

  },
  deleteBook(state, payload) {

    let index = state.books.findIndex(book => {
      return book.id == payload.id
    })

    return state.books.splice(index, 1)

  },
  deleteVideo(state, payload) {

    let index = state.videos.findIndex(video => {
      return video.id == payload.id
    })

    return state.videos.splice(index, 1)

  },
  updateStats(state, payload) {
    return state.stats = payload
  },
  removeUserFromStats(state, payload) {

    let index = state.stats.users.findIndex(user => {
      return payload == user.id
    })

    return state.stats.users.splice(index, 1)

  },
  updateVisitors(state, payload) {
    return state.visitors = payload
  }
}