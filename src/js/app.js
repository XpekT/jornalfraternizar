import Vue from 'vue'
import App from './App.vue'
import SimpleVueValidation from 'simple-vue-validator'
import VueResource from 'vue-resource'
import store from './store'
import VueQuillEditor from 'vue-quill-editor'
import VueScrollTo from 'vue-scrollto'

// Production Values
// Vue.config.devtools = false
// Vue.config.debug = false
// Vue.config.silent = true

// Components
import Navbar from './components/Navbar.vue'
import Loading from './components/Loading.vue'
import Search from './components/Search.vue'
import Modal from './components/Modal.vue'

// Initiate Vue Plugins
Vue.use(VueResource)
Vue.use(SimpleVueValidation)
Vue.use(VueQuillEditor)
Vue.use(VueScrollTo)

// Get and set active page
let page = window.location.pathname
localStorage.setItem('page', page)
store.commit('changePage', page)

// Facebook SDK
window.fbAsyncInit = function() {
    FB.init({
        appId            : '648554598676295',
        autoLogAppEvents : true,
        xfbml            : true,
        version          : 'v2.9'
    });
    FB.AppEvents.logPageView();
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pt_PT/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Twitter SDK
window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));

// Set the CSRF token, Loading State and get User Session if true
window.onload = function() {

  let csrf_name = $('#csrf_name').attr('value')
  let csrf_value = $('#csrf_value').attr('value')

  store.commit('changeToken', {
    'csrf_name': csrf_name,
    'csrf_value': csrf_value
  })

  Vue.http.get("/users/session").then(response => {
    
    store.commit('changeUser', response.data)

  })

  if(page == '/home') {

    setTimeout(() => {

      Vue.http.get("/visitors").then(response => {
        store.commit('updateVisitors', response.data.visitors)
      })

    }, 5000)
  
  }

  store.commit('changeLoading', false)

  // Used jQuery to show the footer because the Twig filesystem loader overrides de v-cloak function of Vue
  // and loads the footer before the page is done loading.
  return $('#footer').show()

}

// Scroll to top
window.onscroll = function(e) {
  
  let body = e.target.body
  let scrollToTopButton = document.getElementById('scroll-to-top')

  if(body.scrollTop >= 600) {
    scrollToTopButton.style.display = 'flex'
  } else {
    scrollToTopButton.style.display = 'none'
  }

}

new Vue({
  store,
  components: { App, Navbar, Loading, Search, Modal }
}).$mount('#app')